/* eslint-disable no-async-promise-executor */
/* eslint-disable no-use-before-define */
/* eslint-disable no-console */
const puppeteer = require('puppeteer');

(() => {
  const attepmt = 0;
  scrap(attepmt)
    .then((att) => { console.log(att); process.exit(); })
    .catch((e) => console.log(e))
    .finally(() => console.log('Program finished'));
})();

async function scrap(attepmt) {
  return new Promise(async (resolve, reject) => {
    const browser = await puppeteer.launch({ headless: false }); // false para ver el Chromium
    const page = await browser.newPage();
    try {
      // await page.goto('https://d3ap0bcke6tef.cloudfront.net/?pg=fnol');
      // await page.goto('http://localhost:8115?pg=fnol');
      await page.goto('http://192.168.1.46:8115/?pg=fnol');
      await wait(3500);
      await page.$eval('body > ion-app > ion-router-outlet > urgent-assistance > page > ion-content > div.page > div.content-wrapper > one-column > ion-grid > ion-row > ion-col > div > urgent-assistance > ion-card > ion-card-content > div.padding-top-l.responsive-buttons > ion-button:nth-child(1)', (el) => el.click());
      await wait(3500);
      await page.$eval('body > ion-app > ion-router-outlet > submit-claim-steps > ion-content > div > div > ion-grid > ion-row:nth-child(2) > ion-col.main.md.hydrated > claim-type > ion-card > div > ion-card-content > ion-row:nth-child(1) > ion-col > div > ion-icon.padding-right-m.fmm-grey2.ion-md-verti-no-check.md.hydrated', (el) => el.click());
      await page.$eval('body > ion-app > ion-router-outlet > submit-claim-steps > ion-content > div > div > ion-grid > ion-row:nth-child(2) > ion-col.main.md.hydrated > ion-grid > ion-row > ion-col.margin-left-auto.no-padding.md.hydrated > ion-button', (el) => el.click());
      await wait(3500);
      console.log(page.url());
    } catch (error) {
      console.error(error);
      reject(error);
    } finally {
      if (page.url().endsWith('result')) {
        await browser.close();
        const att = attepmt + 1;
        console.log(`intento num: ${att}`);
        scrap(att).then((data) => resolve(data)).catch((err) => reject(err));
      } else {
        reject();
      }
    }
  });
}

function wait(time) {
  return new Promise((res) => setTimeout(res, time));
}
